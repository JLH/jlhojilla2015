from flask import Blueprint
from flask import jsonify
from flask import request

mod = Blueprint('api', __name__)

# sample_data = [
#         {'author':'Ali', 'post':'Lorem ipsum', 'date':'October 22 2015', 'gender': 'Not Specified'},
#         {'author':'Ruth', 'post':'Dolor sit amet', 'gender':'Female'},
#         {'author':'Andrew', 'post':'Consectetur Adipiscing Elit', 'gender': 'Not Specified'},
#     ]

sample_data = [
        {'product_name':'Ham', 'price':'50'},
        {'product_name':'Tocino', 'price':'100'},
        {'product_name':'Hotdog', 'price':'150'},
    ]


@mod.route('/getsampledata', methods=['GET','POST'])
def get_sample_data():
    print request.form.keys()
    if request.method=='POST':
        sample_data.append({
            'product_name':request.form['product_name'], 
            'price':request.form['price'], 
        })
    return jsonify(posts=sample_data)