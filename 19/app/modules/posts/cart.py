from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('cart',__name__)
@mod.route('/')
def product_list():
    cart = g.cartdb.getInventory(session['username'])
    return render_template('posts/post.html', cart=cart)

@mod.route('/', methods=['POST'])
def add_product():
    product = request.form['product']    
    price = request.form['price']
    g.postsdb.addProduct(product,price, session['username'])
    return redirect(url_for('.product_list'))


