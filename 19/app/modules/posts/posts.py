from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)
@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    return render_template('posts/post.html', posts=posts)

@mod.route('/', methods=['POST'])
def add_product():
    product = request.form['product']    
    price = request.form['price']
    g.postsdb.addProduct(product,price, session['username'])
    flash('New product added!', 'create_post_success')
    return redirect(url_for('.post_list'))

@mod.route('/', methods=['POST'])
def update_post():
    product = request.form['product']    
    price = request.form['price']
    g.postsdb.updateProduct(product,price, session['username'])
    flash('Product Updated!', 'Succesfully updated 1 record.')
    return redirect(url_for('.post_list'))

# @mod.route('/')
# def inventory():
#     products = g.productsdb.getInventory(session['username'])
#     return render_template('posts/post.html', cart=posts)

# @mod.route('/', methods=['POST'])
# def update_products():
#     product = request.form['product']    
#     price = request.form['price']
#     g.postsdb.updateProduct(product,price, session['username'])
#     flash('Updated New Product!', 'create_post_success')
#     return redirect(url_for('.post_list'))

