from bson.objectid import ObjectId
class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})
    def addProduct(self,product,price, username):
        self.conn.insert({'product':product,'price':price, 'username': username})
    def updateProduct(self, product, price, username):
        self.conn.update({"_id":str(ObjectId(u'_id'))},{"$set":{'product':product,'price':price, 'username': username}})
