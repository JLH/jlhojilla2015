class CartDB:
    def __init__(self, conn):
        self.conn = conn

    def getInventory(self, username):
        return self.conn.find({'username': username})
