class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, emotion, username):
        self.conn.insert({'post':post,'emotion':emotion, 'username': username})